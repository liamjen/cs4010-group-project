from model.MODISHDFDownloader import MODISHDFDownloader


class HDFDownloadProgressListener(MODISHDFDownloader.DownloadProgressListener):
    def fire(self, download_percentage):
        """
        Method stub to be used with GUI to show progress percentage
        :param download_percentage: percentage downloaded from model.MODISHDFDownloader
        """
        print 'Downloading: ' + str(download_percentage) + "%"
