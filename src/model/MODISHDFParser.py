from pyhdf.SD import *
import os
from datetime import datetime


class MODISHDFParser:
    DATE_FORMAT = '%Y%j%H%M'  # Year, julian day (0 - 365), hour, minute

    # Pertinent MODIS Scientific Dataset names
    MODIS_WATER_VAPOR_INFRARED_DATASET_NAME = 'Water_Vapor_Infrared'
    MODIS_WATER_VAPOR_NEAR_INFRARED_DATASET_NAME = 'Water_Vapor_Near_Infrared'
    MODIS_LATITUDE_DATASET_NAME = 'Latitude'
    MODIS_LONGITUDE_DATASET_NAME = 'Longitude'

    def __init__(self, hdf_file_path):
        self.hdf_file_path = hdf_file_path
        self.data_set = SD(hdf_file_path, SDC.READ)
        """
        Get date and time from hdf filename
        
        Example filename:
            MOD05_L2.AYYYYDDD.HHMM.VVV.YYYYDDDHHMMSS.hdf
            
                MOD05_L2      => Earth Science data type name
                YYYYDDD.HHMM  => Year and julian date of data acquisition
                VVV           => Collection version
                YYYYDDDHHMMSS => Processing date/time
                .hdf          => File extension
        """

        hdf_file_name = os.path.basename(hdf_file_path)
        self.data_acquisition_datetime = self.datetime_acquisition_from_file_name(hdf_file_name)

    def __repr__(self):
        return "%s: %s -> %s" % (self.__class__.__name__, str(self.data_acquisition_datetime), self.hdf_file_path)

    def __enter__(self):
        """
        Creates context manager for this class.
        Called at beginning of 'with' block
        """
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Ends context manager
        Called at the end of 'with' block
        """
        self.data_set.end()

    @staticmethod
    def datetime_acquisition_from_file_name(hdf_file_name):
        hdf_info = hdf_file_name.split('.')
        data_acquisition_datetime_julian = str(hdf_info[1][1:]) + str(hdf_info[2])
        return datetime.strptime(data_acquisition_datetime_julian, MODISHDFParser.DATE_FORMAT)

    @staticmethod
    def data_type_name_from_file_name(hdf_file_name):
        return hdf_file_name.split('.')[0]

    @property
    def water_vapor_infrared_dataset(self):
        """
        Water vapor infrared MODIS hdf dataset
        :return: water vapor infrared dataset numpy n-dimensional array
        """
        return self._calculate_actual_SDS_values(self.MODIS_WATER_VAPOR_INFRARED_DATASET_NAME)

    @property
    def water_vapor_near_infrared_dataset(self):
        """
        Water vapor near infrared MODIS hdf dataset
        :return: water vapor near infrared dataset numpy n-dimensional array
        """
        return self._calculate_actual_SDS_values(self.MODIS_WATER_VAPOR_NEAR_INFRARED_DATASET_NAME)

    @property
    def latitude_dataset(self):
        """
        Water latitude MODIS hdf dataset
        :return: latitude dataset numpy n-dimensional array
        """
        return self._calculate_actual_SDS_values(self.MODIS_LATITUDE_DATASET_NAME)

    @property
    def longitude_dataset(self):
        """
        Water vapor infrared MODIS hdf dataset
        :return: longitude dataset numpy n-dimensional array
        """
        return self._calculate_actual_SDS_values(self.MODIS_LONGITUDE_DATASET_NAME)

    def _calculate_actual_SDS_values(self, dataset_name):
        """
        Private method

        Calculate actual scientific dataset numpy n-dimensional array values

        Actual SDS value[x, y] =
            value[x, y] = scale_factor * ( stored_value[x, y] - add_offset )

        :return: ndarray of actual MODIS hdf data granule values
        """
        dataset = self.data_set.select(dataset_name)
        dataset_ndarray = dataset.get()

        # Calculations split into if statements to bypass loops where calculations are not necessary
        if dataset.add_offset != 0.0:
            dataset_ndarray = dataset_ndarray - dataset.add_offset

        if dataset.scale_factor != 1.0:
            dataset_ndarray = dataset_ndarray * dataset.scale_factor

        return dataset_ndarray
