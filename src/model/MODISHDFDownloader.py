import csv
import urllib2
import re
import os
from datetime import datetime


class FileExistsException(Exception):
    """
    Custom Exception for handling an attempt to download HDF files
    """
    def __init__(self):
        super(FileExistsException, self).__init__('HDF File already exists')


class MODISHDFDownloader:
    ARCHIVE_URL = "https://ladsweb.modaps.eosdis.nasa.gov"
    DEFAULT_HDF_FILE_DOWNLOAD_PATH = '../res/hdf_data/'

    def __init__(self, csv_file_path, hdf_file_download_path=DEFAULT_HDF_FILE_DOWNLOAD_PATH):
        """
        Class to handle downloading MODIS HDF file data from a csv.
        CSV file supplied from: https://ladsweb.modaps.eosdis.nasa.gov/search/
        :param csv_file_path: path to csv file containing HDF archive links
        """
        self.csv_data = []
        self.hdf_file_download_path = hdf_file_download_path

        # Listeners for download progress percentage
        self._listeners = []

        with open(csv_file_path, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)

            # Format csv data by date/time
            def datetime_compare(csv_row):
                row_split = csv_row[1].split('.')
                return datetime.strptime(str(row_split[1][1:] + row_split[2]), '%Y%j%H%M')

            next(csv_reader)  # Skip header line of csv

            for r in sorted(csv_reader, key=(lambda x: datetime_compare(x))):
                self.csv_data.append(r)

    def download_at_index(self, index, download_block_size_bytes=524288):
        """
        Download HDF file at index (line number) of csv file
        :param index: Index (int) to download HDF file
        :param download_block_size_bytes: How many bytes to download before notifying download listeners
        :return relative file path to downloaded HDF file
        """
        download_url = str(MODISHDFDownloader.ARCHIVE_URL) + str(self.csv_data[int(index)][1])

        # If file already in folder, do not download
        if download_url.split('/')[-1] in os.listdir(self.hdf_file_download_path):
            raise FileExistsException()

        print 'Downloading file from ' + str(download_url)
        url_open = urllib2.urlopen(download_url)
        file_metadata = url_open.info()

        file_size_bytes = int(file_metadata['content-length'])

        file_name_header = file_metadata['content-disposition']
        file_name_regex_search = re.search('filename=\"(.+)\"', file_name_header)  # Regex search for filename
        if file_name_regex_search:
            with open(self.hdf_file_download_path + str(file_name_regex_search.group(1)), 'wb') as hdf_file:
                file_download_buffer = url_open.read(download_block_size_bytes)
                total_downloaded_bytes = 0
                while file_download_buffer:
                    hdf_file.write(file_download_buffer)
                    total_downloaded_bytes += download_block_size_bytes
                    file_download_buffer = url_open.read(download_block_size_bytes)

                    # Notify all listeners of the percentage downloaded
                    for download_listener in self._listeners:
                        # Download percentage returned as minimum of 100 or actual percent downloaded rounded by 1 place
                        download_listener.fire((min(float(100), round(float(total_downloaded_bytes) * 100.0 / float(file_size_bytes), 1))))

                return hdf_file.name
        else:
            raise IOError('Could not find HDF file')

    class DownloadProgressListener(object):
        """
        "Interface" for implementing method to listen to HDF file download progress
        """
        def fire(self, download_percentage):
            raise NotImplementedError('fire method not implemented in listener')

    def add_listener(self, listener):
        """
        Add DownloadProgressListener implementing class to listen to download progress percentage
        :param listener: listener to add to this MODISHDFDownloader
        """
        self._listeners.append(listener)
